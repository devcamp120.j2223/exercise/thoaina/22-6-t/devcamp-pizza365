import { Component } from "react";

class SizeChoose extends Component {
    render() {
        return (
            <div id="plans" class="row">
                <div className="col-12">
                    <div className="row">
                        <div class="col-sm-12 text-center text-warning p-4 mt-4">
                            <h2><b class="p-1 border-bottom">Menu Combo Pizza 365</b></h2>
                            <p><span class="p-2">Hãy chọn cỡ pizza phù hợp với bạn!</span></p>
                        </div>
                    </div>
                    <div className="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-header bg-dark text-white text-center">
                                            <h3>S (small)</h3>
                                        </div>
                                        <div class="card-body text-center">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item"><b>20cm</b> Đường kính</li>
                                                <li class="list-group-item"><b>2</b> Sườn nướng</li>
                                                <li class="list-group-item"><b>200g</b> Salad</li>
                                                <li class="list-group-item"><b>2</b> Nước ngọt</li>
                                                <li class="list-group-item">
                                                    <h1><b>150.000</b></h1> VND
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="card-footer text-center bg-warning">
                                            <button class="btn btn-info border-white" id="btn-choose-small"
                                                onclick="">Chọn</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-header bg-secondary text-white text-center">
                                            <h3>M (medium)</h3>
                                        </div>
                                        <div class="card-body text-center">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item"><b>25cm</b> Đường kính</li>
                                                <li class="list-group-item"><b>4</b> Sườn nướng</li>
                                                <li class="list-group-item"><b>300g</b> Salad</li>
                                                <li class="list-group-item"><b>3</b> Nước ngọt</li>
                                                <li class="list-group-item">
                                                    <h1><b>200.000</b></h1> VND
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="card-footer text-center bg-warning">
                                            <button class="btn btn-info border-white" id="btn-choose-medium"
                                                onclick="">Chọn</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-header bg-dark text-white text-center">
                                            <h3>L (large)</h3>
                                        </div>
                                        <div class="card-body text-center">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item"><b>30cm</b> Đường kính</li>
                                                <li class="list-group-item"><b>8</b> Sườn nướng</li>
                                                <li class="list-group-item"><b>500g</b> Salad</li>
                                                <li class="list-group-item"><b>4</b> Nước ngọt</li>
                                                <li class="list-group-item">
                                                    <h1><b>250.000</b></h1> VND
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="card-footer text-center bg-warning">
                                            <button class="btn btn-info border-white" id="btn-choose-large"
                                                onclick="">Chọn</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SizeChoose;