import { Component } from "react";
import SlideIntroduce from "./slide/SlideIntroduce";
import TextIntroduce from "./text/TextIntroduce";

class IntroduceComponent extends Component {
    render() {
        return (
            <div>
                <SlideIntroduce />
                <TextIntroduce />
            </div>
        )
    }
}

export default IntroduceComponent;