import { Component } from "react";

class TextIntroduce extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <div className="row">
                        < div class="col-sm-12 text-center p-4 mt-4" >
                            <h2><b class="p-2 border-bottom text-warning">Tại sao lại Pizza 365:</b></h2>
                        </ div>
                    </div>
                    <div className="row">
                        < div class="col-sm-12" >
                            <div class="row">
                                <div class="col-sm-3 p-4 bg-warning text-white border rounded-circle text-center">
                                    <h3 class="p-2">Design</h3>
                                    <p class="p-2">Thiết kế của quán sang trọng, gần gũi với thiên nhiên, đem lại cho người
                                        dùng cảm giác thư giãn và thoải mái khi thưởng thức các món ăn tại đây.</p>
                                </div>
                                <div class="col-sm-3 p-4 bg-warning text-white border rounded-circle text-center">
                                    <h3 class="p-2">Branding</h3>
                                    <p class="p-2">Thương hiệu nổi tiếng xuất xứ từ nước Ý.</p>
                                </div>
                                <div class="col-sm-3 p-4 bg-warning text-white border rounded-circle text-center">
                                    <h3 class="p-2">Consultation</h3>
                                    <p class="p-2">Hợp tác với nhiều doanh nghiệp trên thế giới nói chung và cả nước nói
                                        riêng.</p>
                                </div>
                                <div class="col-sm-3 p-4 bg-warning text-white border rounded-circle text-center">
                                    <h3 class="p-2">Promises</h3>
                                    <p class="p-2">Nhân viên phục vụ tận tình, chu đáo và sẽ không làm người dùng thất vọng.
                                    </p>
                                </div>
                            </div>
                        </ div >
                    </div>
                </div>
            </div>
        )
    }
}

export default TextIntroduce;