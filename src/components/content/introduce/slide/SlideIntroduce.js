import monY1Image from "../../../../assets/images/mon_y_1.jpg";
import monY2Image from "../../../../assets/images/mon_y_2.jpg";
import monY3Image from "../../../../assets/images/mon_y_3.jpg";
import monY4Image from "../../../../assets/images/mon_y_4.jpg";
import monY5Image from "../../../../assets/images/mon_y_5.jpg";

import { Component } from "react";
class SlideIntroduce extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <div className="row">
                        < div class="col-sm-12 text-center text-warning" >
                            <h1>Pizza 365</h1>
                            <h1>Truly italian!</h1>
                        </ div >
                    </div>
                    <div className="row">
                        <div div class="col-sm-12" >
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src={monY1Image} alt="First slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src={monY2Image} alt="Second slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src={monY3Image} alt="Third slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src={monY4Image} alt="Fourth slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src={monY5Image} alt="Fifth slide" />
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SlideIntroduce;