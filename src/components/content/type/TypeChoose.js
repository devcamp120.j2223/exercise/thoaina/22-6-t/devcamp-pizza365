import { Component } from "react";

import pizzaType1 from "../../../assets/images/pizza_type_1.jpg";
import pizzaType2 from "../../../assets/images/pizza_type_2.jpg";
import pizzaType3 from "../../../assets/images/pizza_type_3.jpg";

class TypeChoose extends Component {
    render() {
        return (
            <div id="about" class="row">
                <div class="col-12">
                    <div className="row">
                        <div class="col-sm-12 text-center text-warning p-4 mt-4">
                            <h2><b class="p-2 border-bottom">Chọn loại Pizza</b></h2>
                        </div>
                    </div>
                    <div className="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="card w-100 card-type-pizza">
                                        <img src={pizzaType1} class="card-img-top" />
                                        <div class="card-body text-center bg-warning text-white rounded">
                                            <h3>Hawai</h3>
                                            <p> Sam Panopoulos, a Greek-born
                                                Canadian, created the first Hawaiian pizza at the Satellite Restaurant in
                                                Chatham, Ontario, Canada in 1962.</p>
                                            <p><button class="btn btn-info w-100 border-white" id="btn-hawai-pizza"
                                                onclick="">Chọn</button></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card w-100 card-type-pizza">
                                        <img src={pizzaType2} class="card-img-top" />
                                        <div class="card-body text-center bg-warning text-white rounded">
                                            <h3>Hải sản</h3>
                                            <p>Seafood pizza is pizza prepared with seafood as a primary ingredient. Many
                                                types of seafood ingredients in fresh, frozen or canned forms may be used on
                                                seafood pizza.</p>
                                            <p><button class="btn btn-info w-100 border-white" id="btn-seefood-pizza"
                                                onclick="">Chọn</button></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card w-100 card-type-pizza">
                                        <img src={pizzaType3} class="card-img-top" />
                                        <div class="card-body text-center bg-warning text-white rounded">
                                            <h3>Thịt hun khói</h3>
                                            <p>Herb and Tomato Bacon Pizza - homemade pizza dough topped with bacon, cheese,
                                                tomatoes, and herbs is the perfect pizza to make tonight for the whole
                                                family!</p>
                                            <p><button class="btn btn-info w-100 border-white" id="btn-bacon-pizza"
                                                onclick="">Chọn</button></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TypeChoose;