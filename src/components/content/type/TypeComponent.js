import { Component } from "react";
import TypeChoose from "./TypeChoose";

class TypeComponent extends Component {
    render() {
        return (
            <TypeChoose />
        )
    }
}

export default TypeComponent;