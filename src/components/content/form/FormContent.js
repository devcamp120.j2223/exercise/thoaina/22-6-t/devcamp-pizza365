import { Component } from "react";

class FormContent extends Component {
    render() {
        return (
            <div id="contact" class="row">
                <div class="col-sm-12 text-center text-warning p-4 mt-4">
                    <h2><b class="p-2 border-bottom">Gửi đơn hàng</b></h2>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="fullname">Họ và tên</label>
                                <input type="text" class="form-control" id="inp-fullname" placeholder="Họ và tên" />
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="inp-email" placeholder="Email" />
                            </div>
                            <div class="form-group">
                                <label for="dien-thoai">Điện thoại</label>
                                <input type="text" class="form-control" id="inp-dien-thoai"
                                    placeholder="Điện thoại" />
                            </div>
                            <div class="form-group">
                                <label for="dia-chi">Địa chỉ</label>
                                <input type="text" class="form-control" id="inp-dia-chi" placeholder="Địa chỉ" />
                            </div>
                            <div class="form-group">
                                <label for="message">Lời nhắn</label>
                                <input type="text" class="form-control" id="inp-message" placeholder="Lời nhắn" />
                            </div>
                            <div class="form-group">
                                <label for="voucher">Mã giảm giá (voucher ID)</label>
                                <input type="text" class="form-control" id="inp-voucher" placeholder="Voucher" />
                            </div>
                            <button type="button" class="btn btn-info w-100 text-white"
                                onclick="onBtnSendClick()">Kiểm tra đơn</button>
                        </div>
                    </div>
                </div>
                <div id="div-container-order" class="container bg-info p-2 jumbotron mt-4">
                    <div id="div-order-infor" class="text-white p-3">...</div>
                    <div class="p-2">
                        <button type="button" class="btn bg-warning text-mist w-100 text-white"
                            onclick="onBtnConfirmClick()">Xác
                            nhận</button>
                    </div>
                </div>
                <div id="div-container-thanks" class="container bg-warning p-2 jumbotron mt-4">
                    <div id="div-message-thanks" class="text-dark p-3">
                        <p>Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là:</p>
                        <p>Mã đơn hàng: <span id="span-show-orderId"></span></p>
                    </div>
                </div>
            </div>
        )
    }
}

export default FormContent; 