import { Component } from "react";

class DrinkChoose extends Component {
    render() {
        return (
            <div id="drink" className="row">
                <div className="col-12">
                    <div className="row">
                        <div class="col-sm-12 text-center p-4 mt-4">
                            <h2><b class="p-2 border-bottom text-mist text-warning">Đồ uống</b></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div className="col-12">
                            <select name="" id="select-drink" class="col-sm-6 form-control mx-auto">
                                <option value="0">Chọn loại đồ uống ...</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <table id="show-drink" class="table mt-4 w-50 table-striped mx-auto">
                                <tr class="bg-shadow text-mist">
                                    <th>Loại nước</th>
                                    <th>Giá</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DrinkChoose;