import { Component } from "react";
import FooterText from "./FooterText";

class FooterComponent extends Component {
    render() {
        return (
            <FooterText />
        )
    }
}

export default FooterComponent;