import { Component } from "react";

class FooterText extends Component {
    render() {
        return (
            < div class="container-fluid bg-warning p-5" >
                <div class="row text-center">
                    <div class="col-sm-12">
                        <h4 class="m-2">Footer</h4>
                        <a href="#" class="btn btn-dark m-3"><i class="fa fa-arrow-up"></i>To the top</a>
                        <div class="m-2 p-2">
                            <i class="m-1 fa fa-facebook-official w3-hover-opacity"></i>
                            <i class="m-1 fa fa-instagram w3-hover-opacity"></i>
                            <i class="m-1 fa fa-snapchat w3-hover-opacity"></i>
                            <i class="m-1 fa fa-pinterest-p w3-hover-opacity"></i>
                            <i class="m-1 fa fa-twitter w3-hover-opacity"></i>
                            <i class="m-1 fa fa-linkedin w3-hover-opacity"></i>
                        </div>
                    </div>
                </div>
            </ div>
        )
    }
}

export default FooterText;