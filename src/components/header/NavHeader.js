import { Component } from "react";

class NavHeader extends Component {
    render() {
        return (
            <div div class="container-fluid bg-light" >
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-warning">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav nav-fill w-100">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#plans">Plans</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#about">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <button class="btn" id="order-detail" href="#"
                                            onclick="">Order Detail</button>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
}

export default NavHeader;