import { Component } from "react";
import NavHeader from "./NavHeader";

class HeaderComponent extends Component {
    render() {
        return (
            <NavHeader />
        )
    }
}

export default HeaderComponent;