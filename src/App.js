import './App.css';
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";

import HeaderComponent from "./components/header/HeaderComponent";
import ContentComponent from './components/content/ContentComponent';
import FooterComponent from './components/footer/FooterComponent';

function App() {
   return (
      <div class="container">
         <HeaderComponent />
         <ContentComponent />
         <FooterComponent />
      </div >
   );
}

export default App;
